<div id="footer">
    <div class="footer-top">	
        <div class="center d-flex flex-wrap align-items-start justify-content-between">
            <div class="footer-1">
                <?php if($logofooter) {?>
                    <div class="logofooter sss d-flex align-items-center">
                        <a href=""><img onerror="this.src='<?=THUMBS?>/154x73x2/assets/images/noimage.png';" src="<?=THUMBS?>/154x73x2/<?=UPLOAD_PHOTO_L.$logofooter['photo']?>"/></a>
                    </div>
                <?php }?>	
                <div class="footer-tit"><?=$footer['ten']?></div>
                <div class="footer-content"><?=htmlspecialchars_decode($footer['noidung'])?></div>
            </div>
            <div class="footer-3">
                <?=$addons->setAddons('footer-map', 'footer-map', 10);?>
            </div>
        </div>
    </div>
    <div class="footer-bottom">
        <div class="centeralign-items-center ">
            <p class="copyright">Copyright© ITC 2022 . Design by Võ Văn Tín</p>
        </div>
    </div>
    <?php /*
    <div class="footer-map">
        <div class="center">
            <div class="title-map">
                <?php foreach ($chinhanh as $q => $w): ?>
                    <h2 class="click-map <?php if($q==0) echo 'active';?>" data-id='<?=$w['id']?>' ><?=$w['ten']?></h2>
                <?php endforeach ?>
            </div>
        </div>
        <div class="load-map"></div>
    </div>
    */ ?>
    <?php /* if($source=='index'){ 
        <?=$addons->setAddons('footer-map', 'footer-map', 10);?>
    } */ ?>
</div>