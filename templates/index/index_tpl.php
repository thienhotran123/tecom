<!-- Sản phẩm nổi bật -->
<div class="bgr-sanphamnoibat">
	<div class="center-1045">
		<div class="main-sanphamnoibat">
			<div class="text-sanphamnoibat">
				<p>SẢN PHẨM</p>
				<img src="assets/img/gach2.png">
			</div>
			<div class="div-sanphamnoibat">
				<?php for($i=0,$count=count($sanphamnb); $i<$count; $i++) { ?>
					<div class="wrap-sanphamnoibat">
						<div class="wrap-sanphamnoibat1">
							<div class="img-sanphamnoibat">
								<a class="text-decoration-none scale-img" href="<?=$sanphamnb[$i][$sluglang]?>" title="<?=$sanphamnb[$i]['ten']?>">
								<img onerror="this.src='<?=THUMBS?>/242x209x2/assets/images/noimage.png';" src="<?=THUMBS?>/242x209x1/<?=UPLOAD_PRODUCT_L.$sanphamnb[$i]['photo']?>" alt="<?=$sanphamnb[$i]['ten']?>">
								</a>
									
								<div class="muangay-giohang">
									<span class="addcart" data-id="<?=$sanphamnb[$i]['id']?>" data-action="addnow"><img src="assets/img/giohangsp.png"><span class="themvaogio">Thêm vào giỏ</span></span>     
								</div>
							</div>
							<div class="ten-gia-sanphamnoibat">
								<div class="text-tensp">
									<h3>
										<a class="text-decoration-none text-split text-split-2" href="<?=$sanphamnb[$i][$sluglang]?>" title="<?=$sanphamnb[$i]['ten']?>">
										<?=$sanphamnb[$i]['ten']?>
										</a>
									</h3>
								</div>
								<div class="text-giasp">
									<?php if($sanphamnb[$i]['giakm']) {?>
										<p class="text-giasp2"><?=$func->format_money($sanphamnb[$i]['giamoi'])?></p>
									<?php }elseif($sanphamnb[$i]['gia']){?>
										<p class="text-giasp2"><?=$func->format_money($sanphamnb[$i]['gia'])?></p>
									<?php }else {?>
										<p class="text-giasp2">Liên hệ</p>
									<?php }?>
								</div>
							</div>
						</div>
					</div>
				<?php }?>
			</div>
			<div class="xemthem-tatcasanphamnoibat">
				<a class="transition <?php if($com=='san-pham') echo 'active'; ?>" href="san-pham" title="<?=sanpham?>">Xem tất cả</a>	
			</div>		
		</div>			
	</div>				
</div>

<!-- Sản phẩm sale -->
<div class="bgr-maylocnuoc">
	<div class="center-1045">
		<div class="main-maylocnuoc">
			<div class="text-maylocnuoc">
				<div class="main-flashsale">
					<div class="center">
						<div class="border-flashsale">
							<div class="title-flashsale">
								<div class="left-title-flashsale">
									<h2>Sản phẩm giảm giá <span>|</span> Cập nhập sau</h2>
									<div class="time-flashsale">
										<span id="gio-flashsale"></span>
										<b>:</b>
										<span id="phut-flashsale"></span>
										<b>:</b>
										<span id="giay-flashsale"></span>
										<b></b>
									</div>
									<script>
										/*Lấy thời gian tết âm lịch (mily giây)*/
										var ngaytieptheo = new Date(2023, 12, 7, 0, 0, 0).getTime();
										function newYear() {
											/*Lấy thời gian ngày hiện tại (mily giây) */
											var ngayHienTai = new Date().getTime();
											/*Tính thời gian còn lại (mily giây) */
											thoigianConLai = ngaytieptheo - ngayHienTai;
											/*Chuyển đơn vị thời gian tương ứng sang mili giây*/
											var giay = 1000;
											var phut = giay * 60;
											var gio = phut * 60;
											var ngay = gio * 24;
											/*Tìm ra thời gian theo ngày, giờ, phút giây còn lại thông qua cách chia lấy dư(%) và làm tròn số(Math.floor) trong Javascript*/
											// var d = Math.floor(thoigianConLai / (ngay));
											var h = Math.floor((thoigianConLai % (ngay)) / (gio));
											var m = Math.floor((thoigianConLai % (gio)) / (phut));
											var s = Math.floor((thoigianConLai % (phut)) / (giay));
											/*Hiển thị kết quả ra các thẻ Div với ID tương ứng*/
											// document.getElementById("day").innerText = d;
											document.getElementById("gio-flashsale").innerText = h;
											document.getElementById("phut-flashsale").innerText = m;
											document.getElementById("giay-flashsale").innerText = s;
										}
										/*Thiết Lập hàm sẽ tự động chạy lại sau 1s*/
										setInterval(function () {
											newYear()
										}, 1000)
									</script>
								</div>
								<span class="test">Flash sale</span>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="slick-sale">
				<?php for($i=0,$count=count($maylocnuoc); $i<$count; $i++) { ?>
					<div>
						<div class="wrap-tintuc">
							<div class="wrap-sanphamnoibat11">
								<div class="img-sanphamnoibat">
									<a class="text-decoration-none scale-img" href="<?=$maylocnuoc[$i][$sluglang]?>" title="<?=$maylocnuoc[$i]['ten']?>">
									<img onerror="this.src='<?=THUMBS?>/242x209x2/assets/images/noimage.png';" src="<?=THUMBS?>/242x209x1/<?=UPLOAD_PRODUCT_L.$maylocnuoc[$i]['photo']?>" alt="<?=$maylocnuoc[$i]['ten']?>">
									</a>
										<?php if($maylocnuoc[$i]['giakm']) {?>
											<span class="price-per"><?='-'.$maylocnuoc[$i]['giakm'].'%'?></span>
										<?php }?>
									<div class="muangay-giohang">
										<span class="addcart" data-id="<?=$maylocnuoc[$i]['id']?>" data-action="addnow"><img src="assets/img/giohangsp.png"><span class="themvaogio">Thêm vào giỏ</span></span>     
									</div>
								</div>
								<div class="ten-gia-sanphamnoibat">
									<div class="text-tensp">
										<h3>
											<a class="text-decoration-none text-split text-split-2" href="<?=$maylocnuoc[$i][$sluglang]?>" title="<?=$maylocnuoc[$i]['ten']?>">
											<?=$maylocnuoc[$i]['ten']?>
											</a>
										</h3>
									</div>
									<div class="text-giasp">
										<?php if($maylocnuoc[$i]['giakm']) {?>
											<p class="text-giasp2"><?=$func->format_money($maylocnuoc[$i]['giamoi'])?></p>
											<p class="text-giasp1"><?=$func->format_money($maylocnuoc[$i]['gia'])?></p>
										<?php }elseif($maylocnuoc[$i]['gia']){?>
											<p class="text-giasp2"><?=$func->format_money($maylocnuoc[$i]['gia'])?></p>
										<?php }else {?>
											<p class="text-giasp2">Liên hệ</p>
										<?php }?>
									</div>
								</div>
							</div>
						</div>
					</div>
				<?php }?>
			</div>
			<!-- <div class="div-sanphamnoibat">
				<?php for($i=0,$count=count($maylocnuoc); $i<$count; $i++) { ?>
					<div class="wrap-sanphamnoibat">
						<div class="wrap-sanphamnoibat1">
							<div class="img-sanphamnoibat">
								<a class="text-decoration-none scale-img" href="<?=$maylocnuoc[$i][$sluglang]?>" title="<?=$maylocnuoc[$i]['ten']?>">
								<img onerror="this.src='<?=THUMBS?>/242x209x2/assets/images/noimage.png';" src="<?=THUMBS?>/242x209x1/<?=UPLOAD_PRODUCT_L.$maylocnuoc[$i]['photo']?>" alt="<?=$maylocnuoc[$i]['ten']?>">
								</a>
									<?php if($maylocnuoc[$i]['giakm']) {?>
										<span class="price-per"><?='-'.$maylocnuoc[$i]['giakm'].'%'?></span>
									<?php }?>
								<div class="muangay-giohang">
									<span class="addcart" data-id="<?=$maylocnuoc[$i]['id']?>" data-action="addnow"><img src="assets/img/giohangsp.png"><span class="themvaogio">Thêm vào giỏ</span></span>     
								</div>
							</div>
							<div class="ten-gia-sanphamnoibat">
								<div class="text-tensp">
									<h3>
										<a class="text-decoration-none text-split text-split-2" href="<?=$maylocnuoc[$i][$sluglang]?>" title="<?=$maylocnuoc[$i]['ten']?>">
										<?=$maylocnuoc[$i]['ten']?>
										</a>
									</h3>
								</div>
								<div class="text-giasp">
									<?php if($maylocnuoc[$i]['giakm']) {?>
										<p class="text-giasp2"><?=$func->format_money($maylocnuoc[$i]['giamoi'])?></p>
										<p class="text-giasp1"><?=$func->format_money($maylocnuoc[$i]['gia'])?></p>
									<?php }elseif($maylocnuoc[$i]['gia']){?>
										<p class="text-giasp2"><?=$func->format_money($maylocnuoc[$i]['gia'])?></p>
									<?php }else {?>
										<p class="text-giasp2">Liên hệ</p>
									<?php }?>
								</div>
							</div>
						</div>
					</div>
				<?php }?>
			</div>
			<div class="xemthem-tatcasanphammaylocnuoc">
				<a class="transition <?php if($com=='san-pham') echo 'active'; ?>" href="san-pham" title="<?=sanpham?>">Xem tất cả</a>	
			</div>							 -->
		</div>
	</div>									
</div>

<!-- Dự án công trình -->
<!-- <div class="bgr-duan">
	<div class="center-1365">
		<div class="main-duan">
			<div class="text-duan">
				<p>DỰ ÁN CÔNG TRÌNH</p>
				<img src="assets/img/gach2.png">
			</div>
			<div class="slick-duan">
				<?php for($i=0,$count=count($duan); $i<$count; $i++) { ?>
					<div>
						<div class="wrap-duan">
							<div class="img-duan">
								<a class="text-decoration-none scale-img" href="<?=$duan[$i][$sluglang]?>" title="<?=$duan[$i]['ten']?>">
								<img onerror="this.src='<?=THUMBS?>/455x455x1/assets/images/noimage.png';" src="<?=THUMBS?>/455x455x1/<?=UPLOAD_NEWS_L.$duan[$i]['photo']?>" alt="<?=$duan[$i]['ten']?>">
								</a>
								<div class="ten-duan">
									<h3>
										<a class="text-decoration-none text-split text-split-2" href="<?=$duan[$i][$sluglang]?>" title="<?=$duan[$i]['ten']?>">
										<?=$duan[$i]['ten']?>
										</a>
									</h3>
								</div>
							</div>
						</div>
					</div>
				<?php } ?>
			</div>
		</div>
	</div>
</div> -->

<!-- Tin tức -->
<div class="bgr-tintuc">
	<div class="center-1045">
		<div class="main-tintuc">
			<div class="text-tintuc">
				<p>TIN TỨC</p>
				<img src="assets/img/gach1.png">
			</div>
			<div class="slick-tintuc">
				<?php for($i=0,$count=count($tintucnb); $i<$count; $i++) { ?>
					<div>
						<div class="wrap-tintuc">
							<div class="img-tintuc">
								<a class="text-decoration-none scale-img" href="<?=$tintucnb[$i][$sluglang]?>" title="<?=$tintucnb[$i]['ten']?>">
								<img onerror="this.src='<?=THUMBS?>/334x232x1/assets/images/noimage.png';" src="<?=THUMBS?>/334x232x1/<?=UPLOAD_NEWS_L.$tintucnb[$i]['photo']?>" alt="<?=$tintucnb[$i]['ten']?>">
								</a>
							</div>
							<div class="text-imgtt">
								<h3>
									<a class="text-decoration-none text-split text-split-2" href="<?=$tintucnb[$i][$sluglang]?>" title="<?=$tintucnb[$i]['ten']?>">
									<?=$tintucnb[$i]['ten']?>
									</a>
								</h3>
								<p class="text-decoration-none text-split text-split-2"><?=$tintucnb[$i]['mota']?></p>
							</div>
						</div>
					</div>
				<?php } ?>
			</div>
		</div>
	</div>
</div>

<!-- Đối tác -->
<div class="bgr-doitac">
	<div class="center-1033">
		<div class="main-doitac">
			<div class="text-doitaccuachungtoi">
				<p>ĐỐI TÁC CỦA TECOM</p>
				<img src="assets/img/gach1.png">
			</div>
			<div class="slick-doitac">
				<?php for($i=0,$count=count($doitac); $i<$count; $i++) { ?>
					<div>
						<div class="img-doitac">
							<img onerror="this.src='<?=THUMBS?>/201x49x1/assets/images/noimage.png';" src="<?=THUMBS?>/130x40x2/<?=UPLOAD_PHOTO_L.$doitac[$i]['photo']?>" alt="<?=$doitac[$i]['ten']?>">
						</div>
					</div>
				<?php }?>
			</div>
		</div>
	</div>
</div>

